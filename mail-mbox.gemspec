require_relative 'lib/mail/mbox/version'

Gem::Specification.new do |spec|
  spec.name          = "mail-mbox"
  spec.version       = Mail::Mbox::VERSION
  spec.authors       = ["Matt Wallis"]
  spec.email         = ["matt@acumen-it.co.uk"]

  spec.summary       = %q{For reading messages and headers from mbox files.}
  spec.description   = %q{For reading messages and headers from mbox files.}
  spec.homepage      = "https://git.coop/mattshare/mail-mbox"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://git.coop/mattshare/mail-mbox"
  #spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec|features)/}) }
  end

  # The exe directory is for executables to be installed, not to be confused with
  # the bin dir which is for dev utilities shipped with the gem.
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
