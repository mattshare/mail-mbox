require "test_helper"

class Mail::MboxTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Mail::Mbox::VERSION
  end

  def test_mbox_text_output
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    assert IO.binread(mbox_file) == mbox.text
  end

  def test_message_sections
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    mbox.each {|msg|
      assert msg == msg.header_section + msg.body
    }
  end
  def test_delivery_dates
    expect = [
      "Tue, 24 Mar 2015 06:59:32 -0400",
      "Wed, 25 Mar 2015 11:17:43 -0400",
      "Wed, 25 Mar 2015 13:15:44 -0400"
    ]
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    mbox.each_index{|i|
      assert expect[i] == mbox[i].header_value("Delivery-date")
    }
  end
  def test_number_of_headers_named_received
    expect = [3, 1, 1]
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    mbox.each_index{|i|
      assert expect[i] == mbox[i].headers_hash["Received"].size
    }
  end
  def test_number_of_headers_named_content_type
    expect = [1, 1, 1]
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    mbox.each_index{|i|
      assert expect[i] == mbox[i].headers_hash["Content-Type"].size
    }
  end
  def test_existence_of_all_headers
    # Expected headers from first message in test/data/mbox1:
    expect = [
      "X-Mozilla-Status",
      "X-Mozilla-Status2",
      "X-Mozilla-Keys",
      "Return-path",
      "Envelope-to",
      "Delivery-date",
      "Received",
      "DKIM-Signature",
      "MIME-Version",
      "X-Received",
      "In-Reply-To",
      "References",
      "Date",
      "Message-ID",
      "Subject",
      "From",
      "To",
      "Content-Type"]
    mbox_file = "test/data/mbox1"
    mbox = ::Mail::Mbox::Mbox.new(mbox_file)
    assert expect.sort == mbox.first.headers_hash.keys.sort
  end
end
