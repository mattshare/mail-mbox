require "mail/mbox/version"

module Mail
  module Mbox
    class Error < StandardError; end

    # A Message is the String for a particular mail mesage, held exaclty as it is in the mbox.
    # Note that the body of the message may contain characters that should be treated as 8-bit binary.
    class Message < String
      # For info in Internet Message Format (e.g. mail): https://tools.ietf.org/html/rfc5322

      # Creates a new instance of Mail::Mbox::Message
      #
      # @param msg [String] the representation of the message that is contained in the mbox
      def initialize(msg)
        concat(msg)
      end

      # Gets the value of a header
      #
      # @param header_field_name [String] the name of the header field (excluding ':')
      # @return [String] the value of the header
      #
      # If the named header does not exist, the returned value will be the empty string.
      # If the Message contains more than one header with the given name, the returned
      # value will contain them all, separated by "\n" character.
      def header_value(header_field_name)
        (headers_hash[header_field_name] || []).join("\n")
      end

      # Gets the header section of the message, exactly as it appears in the mbox.
      # If it follows correctly https://tools.ietf.org/html/rfc5322, then it will contain
      # only 7-bit US-ASCII characters
      def header_section
        self[0 ... body_index]
      end

      # Gets the body of the message, exactly as it appears in the mbox.
      # WARNING: this should be regarded as binary data. 
      def body
        self[body_index .. -1]
      end

      # Returns a Hash with field names as keys.
      # It's values are arrays of the field values for the given field name.
      # Arrays are needed because some fields can appear in the header section more than once (e.g. "Received")
      def headers_hash
        @headers_hash ||= make_headers
      end

      private def body_index
        # Offset of the start of the body within the entire message
        #
        # From https://tools.ietf.org/html/rfc5322#section-2.1:
        #
        #   The body is simply a sequence of
        #   characters that follows the header section and is separated from the
        #   header section by an empty line (i.e., a line with nothing preceding
        #   the CRLF).

        @body_index ||= (index("\n\r\n") || size)
      end

      private def regex_for_header_definitions
        # From https://tools.ietf.org/html/rfc5322#section-2.2:
        #
        #   Header fields are lines beginning with a field name, followed by a
        #   colon (":"), followed by a field body, and terminated by CRLF.  A
        #   field name MUST be composed of printable US-ASCII characters (i.e.,
        #   characters that have values between 33 and 126, inclusive), except
        #   colon.
        colon = ":".bytes.first

        # regex for everything between 33 and 126, except ':'
        # i.e. everything from 33 to char before colon, and from char after colon to 126:
        /^([#{33.chr}-#{(colon-1).chr}#{(colon+1).chr}-#{126.chr}]+):\s*(.*)/
      end

      private def regex_for_header_continuation_lines
        # From https://tools.ietf.org/html/rfc5322#section-2.2:
        #
        #   For convenience
        #   however, and to deal with the 998/78 character limitations per line,
        #   the field body portion of a header field can be split into a
        #   multiple-line representation; this is called "folding".  The general
        #   rule is that wherever this specification allows for folding white
        #   space (not simply WSP characters), a CRLF may be inserted before any
        #   WSP.
        /^(\s+)(\S.*)/
      end

      private def make_headers

        # There can be more than one of some headers (e.g. "Received")
        # So we store an array of values for each field name:
        res = Hash.new{|h, k| h[k] = []}

        # split header section into non-space lines, with CR removed (if it was there) from the end of each line:
        empty_line_re = /^\s*$/
        lines = header_section.split(/\r*\n/).reject{|x| x =~ empty_line_re}

        current_header_name = nil
        lines.each {|line|
          if m = regex_for_header_definitions.match(line)

            # CASE 1: line is the start of the defn of a header
            # ------
            current_header_name = m[1]
            res[current_header_name].push(m[2])

          elsif m = regex_for_header_continuation_lines.match(line)

            # CASE 2: line is a 'continuation' line of a folded field body:
            # ------
            unless current_header_name
              # Error: not continuing from a defined header field:
              $stderr.puts "-------------------- header_section --------------"
              $stderr.puts header_section
              raise("Continuation line of header body appears before any header field is defined in header section printed above: \"#{line}\"")
            end
            res[current_header_name].last.concat(" #{m[2]}")

          else

            # CASE 3: neither of the above is an error
            # ------
            raise("Ignoring non-space line in header section: [#{line}]")

          end
        }
        # return the Hash we've created:
        res
      end

    end

    # For info on mbox format, see https://en.wikipedia.org/wiki/Mbox
    # A Mbox is an array of Messages:
    class Mbox < Array
      # A line starting with these characters signifies the start of a message:
      MessageStart = "From "

      def initialize(mbox)
        text = IO.binread(mbox)
        raise("Expected first line to be \"#{MessageStart}\"") unless text =~ /^#{MessageStart}/

        # Ignore the initial MessageStart, and split the rest of the file
        # into separate messages
        concat text[MessageStart.size .. -1].split(/\n#{MessageStart}/).map{|s| Message.new(s)}

      end

      def text
        "#{MessageStart}" + join("\n#{MessageStart}")
      end
    end
  end
end
